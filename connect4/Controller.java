package connect4;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
//import javafx.util.Pair;


public class Controller {

    char computer = 'o';
    char human = 'x';
    Connect4Game board = new Connect4Game(3, 3, 3);

    public void play() {
        System.out.println(board);
        while (true) {
            /*for (Connect4Game game :board.allNextMoves(human)) {
                System.out.println(game);
            }*/
            humanPlay();
            System.out.println(board);

            if (board.isWin(human)) {
                System.out.println("Human wins");
                break;
            }
            if (board.isWithdraw()) {
                System.out.println("Draw");
                break;
            }
            computerPlay();
            System.out.println("_____Computer Turn______");
            System.out.println(board);
            if (board.isWin(computer)) {
                System.out.println("Computer wins!");
                break;
            }
            if (board.isWithdraw()) {
                System.out.println("Draw");
                break;
            }
        }

    }

    //         ************** YOUR CODE HERE ************            \\
    private void computerPlay() {
        // this is a random move, you should change this code to run you own code
        //board = board.allNextMoves(computer).get(0);
        board = (Connect4Game) miniMax(board,Integer.MIN_VALUE, Integer.MAX_VALUE,12, true).get(1);

    }

    /**
     * Human plays
     *
     * @return the column the human played in
     */
    private void humanPlay() {
        Scanner s = new Scanner(System.in);
        int col;
        int col1;
        int col2;
        int pl;
        while (true) {

            System.out.print("1-add 2-remove 3-swap ");
            pl = s.nextInt();
            if (pl == 1){
                System.out.print("Enter column: ");
                col = s.nextInt();
                if ((col > 0) && (col - 1 < board.getWidth())) {
                    if (board.play(human, col - 1,'a')) {
                        return;
                    }
                    System.out.println("Invalid Column: Column " + col + " is full!, try agine");
                }
            }
            if (pl == 2){
                System.out.print("Enter column: ");
                col = s.nextInt();
                if ((col > 0) && (col - 1 < board.getWidth())) {
                    if (board.play(human, col - 1,'d')) {
                        return;
                    }
                    System.out.println("Invalid Column: Column " + col + " is full!, try agine");
                }
            }
            if (pl == 3){
                System.out.print("Enter column1: ");
                col1 = s.nextInt();
                System.out.print("Enter column2: ");
                col2 = s.nextInt();
                if ((col1 > 0) && (col1 - 1 < board.getWidth()) && (col2 > 0) && (col2 - 1 < board.getWidth())) {
                    if (board.playSwap(col1-1 ,col2-1)) {
                        return;
                    }
                    System.out.println("Invalid Column: Column  is full!, try agine");
                }
            }
            /*System.out.print("Enter column: ");
            col = s.nextInt();
            System.out.print("Add or delete: ");
            int d = s.nextInt();
            char dir = (d==1) ? 'a' : 'd';
            System.out.println();
            if ((col > 0) && (col - 1 < board.getWidth())) {
                if (board.play(human, col - 1,dir)) {
                    return;
                }
                System.out.println("Invalid Column: Column " + col + " is full!, try agine");
            }
            System.out.println("Invalid Column: out of range " + board.getWidth() + ", try agine");*/
        }
    }

    private List<Object> miniMax(Connect4Game b,int alpha,int beta, int depth,boolean isMaximizing){
        if (b.allNextMoves(computer).isEmpty() || b.isFinished() || depth ==0){
            List<Object> res = new ArrayList<>(3);
            res.add(b.evaluate(computer, depth));
            res.add(b);
            return res;
        }
        if (isMaximizing){
            int maxEval = Integer.MIN_VALUE;
            List<Connect4Game> nexts = b.allNextMoves(computer);
            Connect4Game bestGame = b.allNextMoves(computer).get(0);
            for (Connect4Game g :
                    nexts) {
                int eval =(int) miniMax(g,alpha,beta,depth-1,false).get(0);
                if (eval > maxEval){
                    bestGame = g;
                    maxEval = eval;
                }
                alpha = Math.max(alpha,eval);
                if (beta <= alpha)
                    break;
            }
            List<Object> res =new ArrayList<>();
            res.add(maxEval);
            res.add(bestGame);
            return res;
        }
        else {
            int minEval = Integer.MAX_VALUE;
            List<Connect4Game> nexts = b.allNextMoves(human);
            Connect4Game bestGame = b.allNextMoves(computer).get(0);
            for (Connect4Game g :
                    nexts) {
                int eval =(int) miniMax(g,alpha,beta,depth -1,true).get(0);
                if (eval < minEval){
                    bestGame = g;
                    minEval = eval;
                }
                beta = Math.min(beta,eval);
                if (beta <= alpha)
                    break;
            }
            List<Object> res =new ArrayList<>();
            res.add(minEval);
            res.add(bestGame);
            return res;
        }
    }

    public static void main(String[] args) {
        Controller g = new Controller();
        g.play();
    }

}
